"use strict"
var outcome = main()
console.log('outcome', outcome.elected_candidates)


function main () {

  var parties = ['Red', 'Blue', 'Yellow', 'Green', 'U-1', 'U-2']
  var candidates = ['RUBY,R', 'CRIMSON,C', 'BURGANDY,B', 'CYAN,C', 'NAVY,N', 'BLONDE,B', 'MUSTARD,M', 'CANARY,C', 'HONEY,H', 'EMERALD,E', 'PINE,P', 'MINT,M', 'SILVER,S', 'GREY,G']

  var ticket = {
    'Red': ['RUBY,R', 'CRIMSON,C', 'BURGANDY,B'],
    'Blue': ['CYAN,C', 'NAVY,N'],
    'Yellow': ['BLONDE,B', 'MUSTARD,M', 'CANARY,C', 'HONEY,H'],
    'Green': ['EMERALD,E', 'PINE,P', 'MINT,M'],
    'U-1': ['SILVER,S'],
    'U-2': ['GREY,G']
  }

  var raw_ballots = get_raw_ballots()
  var party_first_collumn = 1, party_last_collumn = 4,
      candidate_first_column = 5,
      informal_column = 19
  var ballots = rank_raw_ballots(
      raw_ballots, ticket, candidates, parties,
      party_first_collumn, party_last_collumn, candidate_first_column,
      informal_column
      )
  var elected_candidates = []


  // https://www.legislation.gov.au/Details/C2016C01022
  // Commonwealth Electoral Act 1918
  var number_of_seats = 4
  console.log('number_of_seats', number_of_seats)
  var number_of_candidates = candidates.length
  console.log('number_of_candidates', number_of_candidates);
  var number_of_votes = ballots.length
  console.log('number_of_votes', number_of_votes)

  // s273(8):  a quota
  // shall be determined by dividing the total number of first preference
  // votes by 1 more than the number of candidates required to be
  // elected and by increasing the quotient so obtained (disregarding
  // any remainder) by 1, and any candidate who has received a number
  // of first preference votes equal to or greater than the quota shall be
  // elected.
  var droop_quota = Math.floor(number_of_votes / (number_of_seats + 1)) + 1
  console.log('droop_quota', droop_quota)

  candidates.push['_exhausted']
  var results = make_results(candidates)

  var round = 0
  round = increment_round(round, results)
  // implement 273(8)
  distribute_primary(results, ballots, round)
  declare_elected(results, elected_candidates, droop_quota, round)
  // redistribute vote of over quota
  while (round < 6 && elected_candidates.length < number_of_seats
      && transfer(results, ballots, droop_quota, round)) {
    round = increment_round(round, results)
      console.log('round', round)
    declare_elected(results, elected_candidates, droop_quota, round)
  }

  var required_candidates = number_of_seats - elected_candidates.length
    console.log('required_candidates', required_candidates)
  if (required_candidates > 0) {
    exclude_lowest(results, required_candidates, droop_quota)
  }

  return {
    elected_candidates: elected_candidates,
    results: results,
  }
}

// transfer any over quota results
function exclude_lowest(results, required_candidates, quota) {
  var in_play_cr = results.filter(in_play).sort(hightest_to_lowest)
  // shortfall: the number of votes that the candidate requires at that stage
  //   in order to reach the quota s273(29)
  // vacancy shortfall: the aggregate of the shortfalls
  //   of that number of leading candidates equal to the number of
  //   remaining unfilled vacancies s273(29)
  var vacancy_shortfall = in_play_cr
    .slice(0, required_candidates)
    .reduce(function(vs, cr) {
      var shortfall = quota - current_tally(cr)
      return vs + shortfall}, 0)
  console.log('vacancy_shortfall', vacancy_shortfall)
  // leading shortfall: the shortfall of the leading in_play candidate 273(29)
  var leading_shortfall = quota - current_tally(in_play_cr[0])
  console.log('leading_shortfall', leading_shortfall)
  // candidate A: the lowest in play candidate with a tally >= vacancy shortfall 273(13A)(a)
  var candidate_A = current_tally(in_play_cr
      .sort(lowest_to_highest)
      .find(function(cr) {
        console.log('cr', cr)
        console.log('current_tally(cr)', current_tally(cr))
        return current_tally(cr) >= vacancy_shortfall}))
  console.log('candidate_A', candidate_A)
  // candidate B: the highest in play candidate, with a tally < candidate A (if exists) and
  //   tally < vacancy shortfall and not a draw 273(13A)(a)
  // candidate C: the highest in play candidate, with a tally < leading shortfall
  //   tally < vacancy shortfall and not a draw 273(13A)(d)(i)
  // If B.tally < leading shortfall then may exclude B and all below 273(13A)c)
  // Else may exclude all with tally < leading shortfall
  // If candidate B tally and has less than leading shortfall exclude them and all those
  //   lower 273(13A)(c)
  // If candidate B has tally  >= leading shortfall and category exists then exclude C
  //   and all lower exists 273(13A)(d)(ii)

}

// transfer any over quota results
function transfer(results, ballots, quota, round) {
  // find the top candidate who is over quota if there is one
  var cr = results.filter(function(cr) {
      return cr.name != '_exhausted'
        && current_tally(cr) > quota})
    .sort(lowest_to_highest).pop()
  if (cr) {
    // s 273(9) the number (if any) of votes in excess of the quota is the surplus votes
    var surplus = cr.tally[round] - quota
    // s237(9)(a)
    var transfer_value = surplus / cr.tally[round]
    console.log(cr.name, 'has surplus', surplus,
        'with transfer value', transfer_value)
    // adjust tally down to quota
    cr.tally[round] = quota
    // for each vote
    // find votes for this candidate and reclaim value
    ballots.forEach(function(ballot) {
      var affected_round = ballot.allocation
        .findIndex(function(position) {return position == cr.name})
      if (affected_round >= 0) {
        var reclaimed = ballot.spent_value[affected_round] * transfer_value
        ballot.spent_value[affected_round] -= reclaimed
        var candidate = next_vote(ballot.votes, results)
        var next_cr = find_candidate_results(results, candidate)
        allocate_vote(next_cr, ballot, transfer_value, round)
        // console.log('reclaimed', reclaimed, 'for', next_cr.name)
      }
    })
  return true // there may be more over quota
  }
  else {
    console.log('done with transfers')
    return false // no more over quota to redistribute
  }
}

// declare any in play candidate elected
function declare_elected(results, elected_candidates, quota, round) {
  results.filter(function(cr) {
    return in_play(cr) && current_tally(cr) >= quota})
  .sort(hightest_to_lowest)
  .forEach(function(cr) {
    elected_candidates.push(cr.name)
    cr.elected_round = round
    console.log('---------> elected', cr.name)
  })
}

// distrube primary voted
function distribute_primary(results, ballots, round) {
  ballots.forEach(function(ballot) {
    var candidate = ballot.votes[0].candidate
    var cr = results.find(function(cr) {return cr.name == candidate})
    if (cr) {
      var value = 1
      allocate_vote(cr, ballot, value, round)
    }
  })
  results.sort(function(a, b) {return a.tally[round] - b.tally[round]})
}

// place a vote and record allocation
function allocate_vote(candidate_result, ballot, value, round) {
  candidate_result.tally[round] += value
  ballot.allocation[round] = candidate_result.name
  // console.log('voting', value, 'for', candidate)
  ballot.spent_value[round] = value
}

function get_raw_ballots() {
  return [
    [1,1,4,2,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [2,1,2,3,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [3,3,1,2,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [4,4,3,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [5,4,1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [6,3,2,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [7,2,3,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [8,4,1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [9,3,4,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [10,2,1,3,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [11,1,2,3,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [12,1,4,2,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [13,2,1,4,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [14,3,1,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [15,1,4,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [16,4,3,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [17,3,1,2,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [18,3,4,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [19,3,4,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [20,2,3,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [21,4,1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [22,3,2,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [23,2,3,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [24,4,1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [25,3,4,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [26,1,4,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [27,4,3,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [28,3,1,2,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [29,3,4,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [30,3,4,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [31,2,3,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [32,4,1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [33,3,2,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [34,2,3,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [35,4,1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [36,3,2,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [37,2,4,3,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [38,2,1,4,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [39,1,2,3,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [40,1,3,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [41,1,2,4,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [42,1,3,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [43,2,1,3,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [44,4,1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [45,4,2,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [46,3,4,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [47,1,4,2,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [48,2,3,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [49,3,1,2,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [50,3,1,2,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [51,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [52,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [53,0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [54,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [55,3,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [56,0,1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [57,2,3,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [58,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [59,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [60,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [61,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [62,0,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [63,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [64,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [65,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [66,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [67,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [68,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [69,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [70,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [71,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [72,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [73,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [74,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [75,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [76,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [77,0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [78,3,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [79,0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [80,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [81,0,0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [82,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [83,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [84,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [85,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [86,0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [87,0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [88,0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [89,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [90,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [91,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [92,3,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [93,4,3,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [94,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [95,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [96,0,0,0,0,1,2,3,4,5,6,7,8,9,12,13,14,10,11,0],
    [97,0,0,0,0,4,5,0,6,7,8,9,0,0,1,2,3,0,10,0],
    [98,0,0,0,0,3,0,0,4,0,0,0,0,0,0,0,0,1,2,0],
    [99,0,0,0,0,6,7,8,9,10,5,4,11,12,1,2,3,13,0,0],
    [100,0,0,0,0,0,0,5,0,0,4,0,0,0,2,3,0,1,6,0],
    [101,0,0,0,0,1,2,3,0,0,0,4,0,0,0,0,0,0,0,0],
    [102,0,0,0,0,1,2,3,4,0,0,0,0,0,0,0,0,0,0,0],
    [103,0,0,0,0,0,0,0,0,0,1,2,0,0,3,4,0,5,6,0],
    [104,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0],
    [105,0,0,0,0,0,0,0,0,0,3,4,5,6,7,8,9,2,1,0],
    [106,0,0,0,0,5,6,7,8,0,0,0,0,0,2,3,4,1,0,0],
    [107,0,0,0,0,0,0,0,0,0,3,4,5,0,6,7,0,2,1,0],
    [108,0,0,0,0,0,0,0,10,11,4,3,2,1,7,6,5,8,9,0],
    [109,0,0,0,0,0,0,0,1,2,0,3,4,0,0,0,0,5,6,0],
    [110,0,0,0,0,0,0,0,0,0,0,0,0,0,2,3,4,1,0,0],
    [111,0,0,0,0,4,5,6,2,3,0,0,0,0,0,0,0,1,0,0],
    [112,0,0,0,0,0,0,0,0,0,1,2,3,0,4,5,6,7,8,0],
    [113,0,0,0,0,1,2,3,4,5,6,7,14,13,12,11,10,9,8,0],
    [114,0,0,0,0,1,0,0,2,0,3,0,0,0,6,0,0,4,5,0],
    [115,0,0,0,0,0,0,0,1,2,0,0,0,0,0,0,0,4,3,0],
    [116,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [117,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [118,0,0,0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [119,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [121,3,1,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [122,3,1,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [123,4,1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [124,4,1,2,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [125,4,1,2,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [126,3,1,2,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [127,3,1,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [128,3,1,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [129,3,4,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [130,4,3,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [131,2,3,1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [132,3,2,1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [133,4,3,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [134,0,0,0,0,3,2,1,0,0,0,0,0,0,6,7,8,4,5,0],
    [135,0,0,0,0,14,13,12,2,1,8,11,10,9,3,5,7,6,4,0],
    [136,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [137,0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [138,0,0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [139,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [140,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [141,1,2,4,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [142,1,3,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [143,0,0,0,0,6,7,8,0,0,9,0,0,0,3,4,5,1,2,0],
    [144,0,0,0,0,0,0,0,0,0,3,4,5,0,0,0,0,2,1,0],
    [145,0,0,0,0,7,6,8,0,0,9,0,0,0,4,5,3,1,2,0],
    [146,0,0,0,0,0,0,0,0,0,3,4,6,7,5,8,9,1,2,0],
    [147,0,0,0,0,0,0,0,3,4,5,6,7,8,0,0,0,2,1,0],
    [148,0,0,0,0,5,6,7,4,3,0,0,0,0,0,0,0,1,2,0],
    [149,0,0,0,0,6,7,8,0,0,0,0,0,0,3,4,5,1,2,0],
    [150,0,0,0,0,6,0,0,4,0,3,0,0,0,5,0,0,1,2,0],
  ]
}

function rank_raw_ballots(
    raw_ballots, ticket, candidates, parties,
    party_first_collumn, party_last_collumn, candidate_first_column,
    informal_column) {
  var ballots = raw_ballots.map(function(ballot, index) {
    // rank votes into a sorted array of candidate voting
    if (!ballot[informal_column]) {
      var above_the_line = ballot
        .slice(party_first_collumn, party_last_collumn+1)
        .map(function(vote, index, full) {
          var rank = {
            vote: vote,
            party: parties[index]
          }
          return rank
        })
        .filter(function(rank) {return rank.vote})
        .sort(function(a,b) {return a.vote-b.vote})
      var below_the_line = ballot
        .slice(candidate_first_column, candidate_first_column+candidates.length)
        .map(function(vote, index, full) {
          var rank = {
            vote: vote,
            candidate: candidates[index]
          }
          return rank
        })
        .filter(function(rank) {return rank.vote})
        .sort(function(a,b) {return a.vote-b.vote})
      if (above_the_line.length && below_the_line.length) {
        ballot[informal_column] = 1
        console.log('#', index, 'declared informal as above and below line')
      }
      else if (above_the_line.length) {
        let vote = 1
        above_the_line.map(function(rank) {
          ticket[rank.party].map(function(candidate) {
            below_the_line.push({
              vote: vote,
              candidate: candidate
            })
            vote++
          })
        })
      }
    }
    else {
      console.log('#', index, 'is informal')
    }
    return {
      votes: below_the_line, // initial ranking
      spent_value: [0], // spent value per round
      allocation: [],
      available: [1]
    }
  })
  return ballots.filter(function(ballot) {return ballot.votes.length})
}

// determine if the candidate is still in play
function in_play(cr) {
  return cr.elected_round == 0 && cr.eliminated_round == 0
}

// return the latest tally
function current_tally(cr) {
  return cr ? cr.tally[cr.tally.length -1] : undefined
}

// sort function with highest current tally last
function lowest_to_highest(cra, crb) {
  return current_tally(cra) - current_tally(crb)
}

// sort function with lowest tally first
function hightest_to_lowest(cra, crb) {
  return current_tally(crb) - current_tally(cra)
}

// increment the counter and carry forth tallys
function increment_round(round, results) {
  round++
  console.log('start of round', round)
  var total = 0
  // bring forward tally
  results.forEach(function(cr) {
    cr.tally[round] = cr.tally[round-1]
    total += cr.tally[round]
  })
  console.log('total votes cast', total)
  return round
}

// make a tallyboard of results
function make_results(candidates) {
  return candidates.map(function(name, index) {
    return {
      name: name,
      tally: [0],
      elected_round: 0,
      eliminated_round: 0
    }
  })
}

// next in play
function next_vote(votes, results) {
  var vote = votes.find(function(vote) {
    var cr = find_candidate_results(results, vote.candidate)
    return in_play(cr)
  })
  return vote ? vote.candidate : null
}

// find result for candidate
function find_candidate_results(results, name) {
  return results.find(function(cr) {return cr.name == name})
}


